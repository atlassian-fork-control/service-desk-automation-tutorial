define("servicedesk/settings/automation/tutorial/modules/rulethen/issue-label-then-action-model", [
    "automation/backbone-brace"
], function (
    Brace
) {

    return Brace.Model.extend({
        namedAttributes: {
            issueLabel: String
        },
        defaults: {
            issueLabel: ""
        }
    });
});