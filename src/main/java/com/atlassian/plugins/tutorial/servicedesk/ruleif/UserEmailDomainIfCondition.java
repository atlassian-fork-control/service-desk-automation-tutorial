package com.atlassian.plugins.tutorial.servicedesk.ruleif;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.servicedesk.api.ExceptionMessage;
import com.atlassian.servicedesk.api.automation.IfConditionException;
import com.atlassian.servicedesk.api.automation.execution.message.helper.UserMessageHelper;
import com.atlassian.servicedesk.spi.automation.ruleif.IfCondition;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * If condition that checks whether a user's email address belongs to a specified domain.
 *
 */
public final class UserEmailDomainIfCondition implements IfCondition
{
    static final String EMAIL_DOMAIN_KEY = "emailDomain";

    private final UserMessageHelper userMessageHelper;

    @Autowired
    public UserEmailDomainIfCondition(
        final UserMessageHelper userMessageHelper)
    {
        this.userMessageHelper = userMessageHelper;
    }

    /**
     * This method is invoked whenever a rule that contains a user email domain if condition is executed.
     * If this method returns anything other than an {@literal true}, then rule execution halts, and any
     * then actions defined as part of the rule will not be invoked.
     *
     * @param ifConditionParam contains all the contextual information required by the if condition to do its job.
     * @return whether the condition has been met or not
     */
    @Override
    public boolean matches(final IfConditionParam ifConditionParam)
    {
        // Get the email domain we want to check for
        final Optional<String> emailDomainOpt = ifConditionParam.getConfiguration().getData().getValue(EMAIL_DOMAIN_KEY);
        if (!emailDomainOpt.isPresent())
        {
            throw new IfConditionException(new ExceptionMessage("sd.automation.email.config.missing", "No " + EMAIL_DOMAIN_KEY + " property in config data"));
        }

        final String emailDomainToCheckFor = emailDomainOpt.get();
        // Get the email domain of the user that initiated the rule
        ApplicationUser userToCheck = userMessageHelper.getUser(ifConditionParam.getMessage(),
            UserMessageHelper.CURRENT_USER_USER_PREFIX);
        final String userEmailDomain = getEmailDomain(userToCheck);

        // Return the match result
        return userEmailDomain.equalsIgnoreCase(emailDomainToCheckFor);
    }

    private String getEmailDomain(final ApplicationUser fromUser)
    {
        return fromUser.getEmailAddress().substring(
            fromUser.getEmailAddress().indexOf('@') + 1
        );
    }
}