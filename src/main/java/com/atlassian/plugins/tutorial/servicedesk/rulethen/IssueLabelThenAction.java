package com.atlassian.plugins.tutorial.servicedesk.rulethen;

import com.atlassian.jira.bc.issue.label.LabelService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.servicedesk.api.ExceptionMessage;
import com.atlassian.servicedesk.api.automation.ThenActionException;
import com.atlassian.servicedesk.api.automation.execution.message.RuleMessage;
import com.atlassian.servicedesk.api.automation.execution.message.helper.IssueMessageHelper;
import com.atlassian.servicedesk.spi.automation.rulethen.ThenAction;
import java.util.Optional;
import javax.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Adds the label configured in its rule component to the JIRA issue which triggered the rule execution.
 *
 */
public final class IssueLabelThenAction implements ThenAction
{
    static final String ISSUE_LABEL_KEY = "issueLabel";

    private final IssueMessageHelper issueMessageHelper;
    private final LabelService labelService;

    @Autowired
    public IssueLabelThenAction(
        @Nonnull final IssueMessageHelper issueMessageHelper,
        @Nonnull final LabelService labelService)
    {
        this.issueMessageHelper = issueMessageHelper;
        this.labelService = labelService;
    }

    /**
     * Retrieves the label to be added and the issue to add the label to from the supplied {@code thenActionParam}, and
     * adds this label to the issue.
     *
     * If there is any kind of issue or exception, returns a ThenActionError, otherwise returns the provided rule
     * message unmodified.
     */
    @Override
    public RuleMessage invoke(final ThenActionParam thenActionParam)
    {
        // Get the label we want to add to the issue
        final Optional<String> labelOpt = thenActionParam.getConfiguration().getData().getValue(ISSUE_LABEL_KEY);
        if (!labelOpt.isPresent())
        {
            throw new ThenActionException(new ExceptionMessage("sd.automation.label.config.missing", "No " + ISSUE_LABEL_KEY + " property in config data"));
        }

        final String labelToAdd = labelOpt.get();

        final Issue issueToAddLabelTo = issueMessageHelper.getIssue(thenActionParam.getMessage());
        final ApplicationUser userAddingLabel = thenActionParam.getUser();
        addLabelToIssue(labelToAdd, issueToAddLabelTo, userAddingLabel);
        return thenActionParam.getMessage();
    }

    private void addLabelToIssue(final String labelToAdd, final Issue issueToAddLabelTo, final ApplicationUser userAddingLabel)
    {
        final LabelService.AddLabelValidationResult validationResult =
            labelService.validateAddLabel(userAddingLabel, issueToAddLabelTo.getId(), labelToAdd);
        if (!validationResult.isValid())
        {
            throw new ThenActionException(new ExceptionMessage("sd.automation.error.on.label.addition", "Label " + labelToAdd + " couldn't be added to issue"));
        }

        labelService.addLabel(userAddingLabel, validationResult, false);
    }
}