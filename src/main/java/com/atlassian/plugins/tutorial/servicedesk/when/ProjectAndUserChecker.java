package com.atlassian.plugins.tutorial.servicedesk.when;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.servicedesk.api.automation.execution.context.project.ProjectContext;
import com.atlassian.servicedesk.api.automation.execution.whenhandler.WhenHandlerContext;
import com.atlassian.servicedesk.api.automation.execution.whenhandler.WhenHandlerProjectContextService;
import com.atlassian.servicedesk.api.automation.execution.whenhandler.WhenHandlerRunInContextService;
import java.util.List;
import javax.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectAndUserChecker
{
    @Autowired
    private WhenHandlerProjectContextService whenHandlerProjectContextService;
    @Autowired
    private WhenHandlerRunInContextService whenHandlerRunInContextService;
    @Autowired
    private PermissionManager permissionManager;

    /**
     * Checks whether a when handler context matches a given project.
     */
    public boolean isApplicableProject(@Nonnull WhenHandlerContext context,
                                       @Nonnull Project project)
    {
        try {
            final ProjectContext applicationProjectContext = whenHandlerProjectContextService.getApplicationProjectContext(context);
            final List<Project> projects = applicationProjectContext.getProjects();
            if (projects.isEmpty()) {
                return true;
            } else {
                return projects.contains(project);
            }
        } catch (Exception ex) {
            return false;
        }
    }


    /**
     * Can the passed user browse the given issue?
     */
    public boolean canBrowseIssue(@Nonnull WhenHandlerContext context,
                                  @Nonnull final Issue issue)
    {
        return whenHandlerRunInContextService.executeInContext(context, user -> {
            return permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user);
        });
    }
}